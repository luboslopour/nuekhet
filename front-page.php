<?php // Template name: Homepage ?>
<?php get_header() ?>
<?php while(have_rows('content_repeater')): the_row(); ?>

	<?php if(get_sub_field('section')=='header'): ?>
		<header class="col-lg-12 custom-home-main<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>" role="banner">
			<?php
				$background = get_sub_field('background');
				if(!empty($background)) {
					echo '<div class="custom-home-bg" style="background-image:url('.$background['url'].');"></div>';
				}
			?>
			<div class="row custom-window-height">
				<?php
					$image = get_sub_field('image');
					if(!empty($image)) {
						echo '<div class="col-lg-6 d-none d-lg-block cover-image" style="background-image: url('.$image['sizes']['large'].');"></div>';
					}
				?>
				<div class="position-relative align-self-center <?php if(!empty($image)) { echo 'col-lg-6'; } else { echo 'container'; } ?>">
					<div class="bg-danger text-white my-4 p-5">
						<?php
							if(get_sub_field('title')) {
								echo '<h2 class="display-4">'.get_sub_field('title').'</h2>';
							}
							if(get_sub_field('button_url')) {
								echo '<p class="m-0 mt-4"><a '.((get_sub_field('button_target')==true)?'target="_blank" ':'').'href="'.get_sub_field('button_url').'" class="btn btn-light btn-lg text-uppercase px-4 my-2">';
									if(get_sub_field('icon')) {
										echo '<span class="ion-'.get_sub_field('icon').' mr-3 h1 d-inline-block align-middle"></span>';
									}
									if(get_sub_field('button_text')) {
										the_sub_field('button_text');
									} else {
										_e('Read more','Continue reading','theme');
									}
								echo '</a></p>';
							}
						?>
					</div>
				</div>
			</div>
		</header>
	<?php endif; ?>

	<?php if(get_sub_field('section')=='video'): ?>
		<div class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
			<div class="container">
				<div class="row">
					<figure class="col-lg-6 my-4">
						<?php
							$url = get_sub_field('video');
							$regs = array();
							$id = '';
							if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
							    $id = $regs[3];
							}
							echo '<div data-type="vimeo" data-video-id="'.$id.'"></div>';
						?>
						<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/apps/plyr/plyr.css">
						<script src="<?php bloginfo('template_url') ?>/apps/plyr/plyr.js"></script>
						<script>plyr.setup();</script>
					</figure>
					<article class="col-lg-6 my-4">
						<?php
							if(get_sub_field('title')) {
								echo '<h2 class="display-3 text-info">';
									if(get_sub_field('icon')) {
										echo '<span class="d-none d-sm-inline-block ion-'.get_sub_field('icon').' text-secondary mr-3"></span>';
									}
								the_sub_field('title');
								echo '</h2>';
							}
							if(get_sub_field('content')) {
								echo '<p>'.get_sub_field('content').'</p>';
							}
							if(get_sub_field('button_url')) {
								echo '<nav><a '.((get_sub_field('button_target')==true)?'target="_blank" ':'').'href="'.get_sub_field('button_url').'" class="btn btn-info">';
									if(get_sub_field('button_text')) {
										the_sub_field('button_text');
									} else {
										_e('Read more','Continue reading','theme');
									}
								echo '</a></nav>';
							}
						?>
					</article>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if(get_sub_field('section')=='boxes'): ?>
		<section class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
			<div class="container">
				<?php
					if(get_sub_field('title')) {
						echo '<h2 class="display-3 text-lg-center text-primary mb-5">';
							if(get_sub_field('icon')) {
								echo '<span class="d-none d-sm-inline-block ion-'.get_sub_field('icon').' text-muted mr-3"></span>';
							}
						the_sub_field('title');
						echo '</h2>';
					}
					if(have_rows('boxes_repeater')) {
						echo '<div class="row">';
							while(have_rows('boxes_repeater')) {
								the_row();
								echo '<div class="col-lg-4 my-4">';
									if(get_sub_field('boxes_title')) {
										echo '<h2 class="mb-4 text-muted">'.get_sub_field('boxes_title').'</h2>';
									}
									if(get_sub_field('boxes_content')) {
										echo '<p>'.get_sub_field('boxes_content').'</p>';
									}
									if(get_sub_field('boxes_url')) {
										echo '<nav><a '.((get_sub_field('boxes_button_target')==true)?'target="_blank" ':'').'href="'.get_sub_field('boxes_url').'" class="btn btn-primary">';
											if(get_sub_field('boxes_button_text')) {
												the_sub_field('boxes_button_text');
											} else {
												_e('Read more','Continue reading','theme');
											}
										echo '</a></nav>';
									}
								echo '</div>';
							}
						echo '</div>';
					}
				?>
			</div>
		</section>
	<?php endif; ?>

	<?php if(get_sub_field('section')=='feedback'): ?>
		<?php
			$args = array(
				'post_type' => 'feedback',
				'posts_per_page' => 3,
				'orderby' => get_sub_field('order'),
				'order'    => 'DESC',
			);
			query_posts( $args );
		?>
		<?php if(have_posts()): ?>
			<section class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
				<div class="container">
					<?php
						if(get_sub_field('title')) {
							echo '<h2 class="display-3 text-lg-center text-success mb-5">';
								if(get_sub_field('icon')) {
									echo '<span class="d-none d-sm-inline-block ion-'.get_sub_field('icon').' text-secondary mr-3"></span>';
								}
							the_sub_field('title');
							echo '</h2>';
						}
					?>
					<div class="row">
						<?php get_template_part('loop','feedback'); ?>
					</div>
					<nav class="text-lg-center">
						<a href="<?php echo get_post_type_archive_link('feedback') ?>" class="btn btn-success btn-lg">
							<?php
								if(get_sub_field('button_text')) {
									the_sub_field('button_text');
								} else {
									_e('Read more','Continue reading','theme');
								}
							?>
						</a>
					</nav>
				</div>
			</section>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	<?php endif; ?>

	<?php if(get_sub_field('section')=='content_box'): ?>
		<section class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
			<div class="container text-center text-lg-left">
				<div class="row">
					<?php
						$image = get_sub_field('image');
						if(!empty($image)) {
							echo '<figure class="col-6 offset-3 col-md-4 offset-md-4 offset-lg-0 my-0 my-lg-3 align-self-center"><img class="img-fluid img-thumbnail rounded-circle" src="'.$image['sizes']['Square'].'" alt=""></figure>';
						}
					?>
					<div class="<?php if(!empty($image)) { echo 'col-lg-8'; } else { echo 'col-lg-12'; } ?> my-lg-3 p-lg-5 align-self-center">
						<?php
							if(get_sub_field('title')) {
								echo '<h2 class="display-3 text-warning my-2">';
									if(get_sub_field('icon')) {
										echo '<span class="d-none d-sm-inline-block ion-'.get_sub_field('icon').' text-muted mr-4"></span>';
									}
								the_sub_field('title');
								echo '</h2>';
							}
							if(get_sub_field('content')) {
								echo '<p>'.get_sub_field('content').'</p>';
							}
							if(get_sub_field('button_url')) {
								echo '<nav class="my-4"><a '.((get_sub_field('button_target')==true)?'target="_blank" ':'').'href="'.get_sub_field('button_url').'" class="btn btn-warning btn-lg">';
									if(get_sub_field('button_text')) {
										the_sub_field('button_text');
									} else {
										_e('Read more','Continue reading','theme');
									}
								echo '</a></nav>';
							}
						?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<?php if(get_sub_field('section')=='blog'): ?>
		<?php
			$args = array(
				'post_type' => 'blog',
				'posts_per_page' => 3,
				'orderby' => 'date',
				'order'    => 'DESC',
			);
			query_posts( $args );
		?>
		<?php if(have_posts()): ?>
			<section class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
				<div class="container">
					<?php
						if(get_sub_field('title')) {
							echo '<h2 class="display-2 text-center text-danger mb-4">';
								if(get_sub_field('icon')) {
									echo '<span class="d-none d-sm-inline-block ion-'.get_sub_field('icon').' text-secondary mr-4"></span>';
								}
							the_sub_field('title');
							echo '</h2>';
						}
					?>
					<div class="row">
						<?php get_template_part('loop','blog'); ?>
					</div>
					<nav class="py-4 text-center border border-left-0 border-right-0 border-bottom-0">
						<a href="<?php echo get_post_type_archive_link('blog'); ?>" class="btn btn-danger btn-lg">
							<?php
								if(get_sub_field('button_text')) {
									the_sub_field('button_text');
								} else {
									_e('Read more','Continue reading','theme');
								}
							?>
						</a>
					</nav>
				</div>
			</section>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	<?php endif; ?>

	<?php if(get_sub_field('section')=='contact'): ?>
		<div class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
			<div class="container">
				<div class="row">
					<?php
						$image = get_sub_field('image');
						if(!empty($image)) {
							echo '<figure class="col-6 offset-3 col-md-4 offset-md-4 col-lg-3 offset-lg-2 align-self-center"><img class="img-fluid img-thumbnail rounded-circle" src="'.$image['sizes']['Square'].'" alt=""></figure>';
						}
					?>
					<div class="col-lg-6 align-self-center text-center text-lg-left">
						<?php
							if(get_sub_field('title')) {
								echo '<h2 class="display-3 text-primary">';
									if(get_sub_field('icon')) {
										echo '<span class="d-none d-sm-inline-block ion-'.get_sub_field('icon').' text-secondary mr-4"></span>';
									}
								the_sub_field('title');
								echo '</h2>';
							}
							if(get_sub_field('content')) {
								echo '<p>'.get_sub_field('content').'</p>';
							}
						?>
					</div>
				</div>
				<?php if(get_sub_field('contact')) echo '<div class="my-5">'.do_shortcode('[contact-form-7 id="'.get_sub_field('contact').'"]').'</div>'; ?>
			</div>
		</div>
	<?php endif; ?>

<?php endwhile; ?>
<?php get_footer() ?>