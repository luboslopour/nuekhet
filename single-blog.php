<?php
	get_header();
	get_template_part('header','main');
?>
<div class="container">
	<main role="main">
		<div class="row">
			<?php while(have_posts()) : the_post(); ?>
				<div class="col-lg-4">
					<?php if(has_post_thumbnail()): ?>
						<figure class="my-2">
							<a href="<?php the_post_thumbnail_url(); ?>">
								<?php the_post_thumbnail('thumbnail',['class'=>'img-fluid']); ?>
							</a>
						</figure>
					<?php endif; ?>
					<p class="text-muted h5 text-lg-right my-2">
						<span class="ion ion-ios-calendar-outline mr-2"></span>
						<?php the_time('d. m. Y') ?>
					</p>
				</div>
				<div class="col-lg-8 mb-5">
					<h1 class="mb-3"><?php the_title() ?></h1>
					<?php the_content() ?>
				</div>
			<?php endwhile; ?>
		</div>
	</main>
</div>
<?php
	query_posts(array(
		'post_type' => 'blog',
		'posts_per_page' => 3,
		'post__not_in' => array(get_the_ID()),
		'orderby' => 'date',
		'order' => 'DESC',
	));
?>
<?php if(have_posts()): ?>
		<div class="bg-light py-5">
			<div class="container">
				<div class="border border-left-0 border-right-0 border-bottom-0 text-center text-muted h2 pt-4 mb-5"><?php _e('Other posts','Archive Title','theme'); ?></div>
				<div class="row justify-content-center">
					<?php get_template_part('loop','blog'); ?>
				</div>
				<nav class="border border-white border-left-0 border-right-0 border-bottom-0 text-center py-4">
					<a href="" class="btn btn-danger btn-lg">
						Blog archive
					</a>
				</nav>
			</div>
		</div>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>
