<?php while(have_posts()) : the_post(); ?>
	<div class="col-lg-4 mb-4">
		<blockquote class="blockquote">
			<span class="ion ion-ios-quote-outline float-left display-4 mr-4 text-muted"></span>
			<?php echo '<p>'.get_the_excerpt().'</p>'; ?>
			<footer class="blockquote-footer"><cite><?php the_title(); ?></cite></footer>
		</blockquote>
	</div>
<?php endwhile; ?>