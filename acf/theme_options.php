<?php

if(function_exists('acf_add_options_page')){
	acf_add_options_sub_page(array(
		'page_title' 	=> __('Theme Setting','ACF page title','theme'),
		'menu_title' 	=> __('Theme Setting','ACF menu title','theme'),
		'menu_slug' => 'theme-setting',
		'parent_slug' 	=> 'themes.php',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> __('Contacts Setting','ACF page title','theme'),
		'menu_title' 	=> __('Contacts Setting','ACF menu title','theme'),
		'menu_slug' => 'contacts-setting',
		'parent_slug' 	=> 'options-general.php',
	));
}

if(function_exists('acf_add_local_field_group')){
	acf_add_local_field_group(
		array(
			'title' => __('Contacts','ACF field','theme'),
			'key' => 'contact_setting',
			'label_placement' => 'left',
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'contacts-setting',
					),
				),
			),
			'fields' => array(
				array(
					'label' => __('Name','ACF field','theme'),
					'key' => 'company',
					'name' => 'company',
					'type' => 'text',
				),
				array(
					'label' => __('Street','ACF field','theme'),
					'key' => 'street',
					'name' => 'street',
					'type' => 'text',
				),
				array(
					'label' => __('City','ACF field','theme'),
					'key' => 'city',
					'name' => 'city',
					'type' => 'text',
				),
				array(
					'label' => __('ZIP','ACF field','theme'),
					'key' => 'zip',
					'name' => 'zip',
					'type' => 'text',
				),
				array(
					'label' => __('Phone','ACF field','theme'),
					'key' => 'phone',
					'name' => 'phone',
					'type' => 'text',
				),
				array(
					'label' => __('E-mail','ACF field','theme'),
					'key' => 'email',
					'name' => 'email',
					'type' => 'text',
				)
			)
		)
	);
}

if(function_exists('acf_add_local_field_group')){
	acf_add_local_field_group(
		array(
			'title' => __('Images','ACF field','theme'),
			'key' => 'theme_setting',
			'label_placement' => 'left',
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'theme-setting',
					),
				),
			),
			'fields' => array(
				array(
					'label' => __('Main Image','ACF field','theme'),
					'key' => 'main-image',
					'name' => 'main-image',
					'type' => 'image',
				),
				array(
					'label' => __('Background Image','ACF field','theme'),
					'key' => 'main-background',
					'name' => 'main-background',
					'type' => 'image',
				),
			)
		)
	);
}