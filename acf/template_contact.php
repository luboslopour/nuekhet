<?php
if(function_exists('acf_add_local_field_group')){
	acf_add_local_field_group(
		array(
			'title' => __('Contacts','ACF field','theme'),
			'key' => 'template_contact',
			'label_placement' => 'left',
			'hide_on_screen' => array(
				'the_content' => true,
			),
			'location' => array (
				array (
					array (
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'template-contact.php',
					),
				),
			),
			'fields' => array(
				array(
					'label' => __('Form','ACF field','theme'),
					'key' => 'contact_form',
					'name' => 'contact_form',
					'type' => 'post_object',
					'return_format' => 'id',
					'post_type' => array('wpcf7_contact_form'),
				),
				array(
					'label' => __('','ACF field','theme'),
					'type' => 'message',
					'message' => '<a target="_blank" class="button" href="'.admin_url().'/options-general.php?page=contacts-setting">'.__('Edit your contact setting','ACF message','theme').'</a>',
				)
			)
		)
	);
}