<?php

$icons_json = json_decode(file_get_contents(get_bloginfo('template_url').'/fonts/ionicons/data/ionicons.json'),true);
$icons = array();
foreach ($icons_json as $icon) {
	$tags = $icon['tags'];
	foreach($icon['icons'] as $icon) {
		if (strpos($icon['name'], 'ios-') !== false) {
			$icons = array_merge($icons,array($icon['name'] => '&'.str_replace('0xf','#xf',$icon['code']).'; '.str_replace('ios-','',$icon['name'].' ('.implode(' ',$tags).')')));
		}
	}
}



if(function_exists('acf_add_local_field_group')){
	// page content metabox
	acf_add_local_field_group(
		array(
			'title' => __('Page content','ACF field','theme'),
			'key' => 'front_page_content',
			'hide_on_screen' => array(
				'the_content' => true,
			),
			'location' => array (
				array (
					array (
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'front-page.php',
					),
				),
			),
		)
	);
}


if(function_exists('acf_add_local_field')){
	// content repeater
	acf_add_local_field(array(
		'key' => 'content_repeater',
		'name' => 'content_repeater',
		'type' => 'repeater',
		'button_label' => __('Add Section','ACF field','theme'),
		'layout' => 'row',
		'parent' => 'front_page_content',
	));
		acf_add_local_field(array(
			'label' => __('Select Section','ACF field','theme'),
			'key' => 'section',
			'name' => 'section',
			'type' => 'select',
			'choices' => array(
				'' => __('- Choose content type -','ACF select','theme'),
				'header' => __('Header','ACF select','theme'),
				'video' => __('Video','ACF select','theme'),
				'boxes' => __('Boxes','ACF select','theme'),
				'feedback' => __('Feedback','ACF select','theme'),
				'content_box' => __('Content box','ACF select','theme'),
				'blog' => __('Blog','ACF select','theme'),
				'contact' => __('Contact','ACF select','theme'),
			),
			'parent' => 'content_repeater',
		));
			acf_add_local_field(array(
				'label' => __('Video','ACF field','theme'),
				'key' => 'video',
				'name' => 'video',
				'type' => 'url',
				'instructions' => __('Vimeo URL','ACF field','theme'),
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Icon','ACF field','theme'),
				'key' => 'icon',
				'name' => 'icon',
				'type' => 'select',
				'allow_null' => true,
				'ui' => true,
				'choices' => $icons,
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'boxes',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'feedback',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'blog',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'contact',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Title','ACF field','theme'),
				'key' => 'title',
				'name' => 'title',
				'type' => 'text',
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'boxes',
						),
					),

					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'feedback',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'blog',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'contact',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Content','ACF field','theme'),
				'key' => 'content',
				'name' => 'content',
				'type' => 'textarea',
				'rows' => 5,
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'contact',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Button URL','ACF field','theme'),
				'key' => 'button_url',
				'name' => 'button_url',
				'type' => 'url',
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Button text','ACF field','theme'),
				'key' => 'button_text',
				'name' => 'button_text',
				'type' => 'text',
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'feedback',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'blog',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Button target','ACF field','theme'),
				'key' => 'button_target',
				'name' => 'button_target',
				'type' => 'true_false',
				'message' => __('Open link in a new tab','ACF field','theme'),
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Background image','ACF field','theme'),
				'key' => 'background',
				'name' => 'background',
				'type' => 'image',
				'preview_size' => 'medium',
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Image','ACF field','theme'),
				'key' => 'image',
				'name' => 'image',
				'type' => 'image',
				'preview_size' => 'medium',
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'contact',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Boxes','ACF field','theme'),
				'key' => 'boxes_repeater',
				'name' => 'boxes_repeater',
				'type' => 'repeater',
				'button_label' => __('Add Box','ACF field','theme'),
				'layout' => 'row',
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'boxes',
						),
					),
				),
				'parent' => 'content_repeater',
			));
						acf_add_local_field(array(
							'label' => __('Title','ACF field','theme'),
							'key' => 'boxes_title',
							'name' => 'boxes_title',
							'type' => 'text',
							'parent' => 'boxes_repeater',
						));
						acf_add_local_field(array(
							'label' => __('Content','ACF field','theme'),
							'key' => 'boxes_content',
							'name' => 'boxes_content',
							'type' => 'textarea',
							'parent' => 'boxes_repeater',
						));
						acf_add_local_field(array(
							'label' => __('Button URL','ACF field','theme'),
							'key' => 'boxes_url',
							'name' => 'boxes_url',
							'type' => 'url',
							'parent' => 'boxes_repeater',
						));
						acf_add_local_field(array(
							'label' => __('Button text','ACF field','theme'),
							'key' => 'boxes_button_text',
							'name' => 'boxes_button_text',
							'type' => 'text',
							'parent' => 'boxes_repeater',
						));
						acf_add_local_field(array(
							'label' => __('Button target','ACF field','theme'),
							'key' => 'boxes_button_target',
							'name' => 'boxes_button_target',
							'type' => 'true_false',
							'message' => __('Open link in a new tab','ACF field','theme'),
							'parent' => 'boxes_repeater',
						));
						acf_add_local_field(array(
							'label' => __('Image','ACF field','theme'),
							'key' => 'boxes_timage',
							'name' => 'boxes_timage',
							'type' => 'image',
							'preview_size' => 'medium',
							'parent' => 'boxes_repeater',
						));
			acf_add_local_field(array(
				'label' => __('Feedback order','ACF field','theme'),
				'key' => 'order',
				'name' => 'order',
				'type' => 'select',
				'choices' => array(
					'rand' => __('Random','ACF select','theme'),
					'date' => __('By date','ACF select','theme'),
				),
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'feedback',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Contact form','ACF field','theme'),
				'key' => 'contact',
				'name' => 'contact',
				'type' => 'post_object',
				'return_format' => 'id',
				'post_type' => 'wpcf7_contact_form',
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'contact',
						),
					),
				),
				'parent' => 'content_repeater',
			));
			acf_add_local_field(array(
				'label' => __('Background color','ACF field','theme'),
				'key' => 'background-color',
				'name' => 'background-color',
				'type' => 'select',
				'choices' => array(
					'' => __('None','ACF select','theme'),
					'bg-light' => __('Light','ACF select','theme'),
					'bg-light progress-bar-striped' => __('Light (striped)','ACF select','theme'),
					'bg-dark text-white' => __('Dark','ACF select','theme'),
					'bg-dark text-white progress-bar-striped' => __('Dark (striped)','ACF select','theme'),
				),
				'conditional_logic' => array(
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'header',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'video',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'boxes',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'feedback',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'content_box',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'blog',
						),
					),
					array (
						array (
							'field' => 'section',
							'operator' => '==',
							'value' => 'contact',
						),
					),
				),
				'parent' => 'content_repeater',
			));
}