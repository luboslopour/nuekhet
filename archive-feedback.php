<?php
	get_header();
	get_template_part('header','main');
	query_posts(array(
		'post_type' => 'feedback',
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order'    => 'DESC',
	));
?>
<?php if(have_posts()): ?>
	<main role="main">
		<section class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
			<div class="container">
				<?php
					if(get_sub_field('title')) {
						echo '<h2 class="display-3 text-lg-center text-success mb-5">';
							if(get_sub_field('icon')) {
								echo '<span class="d-none d-sm-inline-block ion-'.get_sub_field('icon').' text-secondary mr-3"></span>';
							}
						the_sub_field('title');
						echo '</h2>';
					}
				?>
				<div class="row">
					<?php get_template_part('loop','feedback'); ?>
				</div>
			</div>
		</section>
	</main>
<?php endif; ?>
<?php get_footer(); ?>
