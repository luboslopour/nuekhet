<?php
	// Template name: Contact
	get_header();
	get_template_part('header','main');
?>
<div class="container">
	<main role="main">
		<?php while(have_posts()): the_post(); ?>
			<address class="lead row justify-content-center">
				<div class="col-lg-12 text-center">
					<?php
						if(get_field('company','option')) echo '<p class="h3">'.get_field('company','option').'</p>';
						if(get_field('street','option')) echo get_field('street','option');
						if(get_field('zip','option')) echo ', '.get_field('zip','option');
						if(get_field('city','option')) echo ' '.get_field('city','option');
					?>
				</div>

				<?php if(get_field('phone','option')) echo '<span class="col-lg-6 text-lg-right"><span class="ion-ios-call-outline text-muted h1 m-0 mr-2 d-inline-block align-middle"></span>'.get_field('phone','option').'</span>'; ?>
				<?php if(get_field('email','option')) echo '<span class="col-lg-6"><span class="ion-ios-mail-outline text-muted h1 m-0 mr-3 d-inline-block align-middle"></span>'.get_field('email','option').'</span>'; ?>
			</address>
			<?php
				if(get_field('contact_form')) {
					echo do_shortcode('[contact-form-7 id="'.get_field('contact_form').'"]');
				}
			?>
		<?php endwhile; ?>
	</main>
</div>
<?php get_footer(); ?>
