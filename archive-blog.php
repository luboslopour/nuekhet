<?php
	get_header();
	get_template_part('header','main');
?>
<div class="container">
	<main role="main">
		<?php if(have_posts()): ?>
			<section class="py-5<?php if(get_sub_field('background-color')) echo ' '.get_sub_field('background-color'); ?>">
				<div class="container">
					<div class="row justify-content-center">
						<?php get_template_part('loop','blog'); ?>
					</div>
					<?php
						if (function_exists('bootstrap4_pagination')) {
							bootstrap4_pagination(1,'','center','danger','top');
						}
					?>
				</div>
			</section>
		<?php endif; ?>
	</main>
</div>
<?php get_footer(); ?>
