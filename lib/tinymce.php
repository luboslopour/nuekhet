<?php

function format_tinymce($init) {
	$init['theme_advanced_blockformats'] = 'p,h2,h3,h4';
	return $init;
}
add_filter('tiny_mce_before_init', 'format_tinymce' );
