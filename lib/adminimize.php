<?php



// remove the wordpress update notification for all users except sysadmin
global $user_login;
get_currentuserinfo();
if (!current_user_can('manage_options')) {
	add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
	add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
}



// remove update nag message for all users except sysadmin
function remove_upgrade_nag() {
		if( !current_user_can('update_core') ) {
			echo '<style type="text/css">
			.update-nag {display: none}
			</style>';
	}
}
add_action('admin_head', 'remove_upgrade_nag');



// redirect admin dashboard to pages for all users except sysadmin
if (!current_user_can('manage_options')) {
	function dashboard_redirect(){
		wp_redirect(admin_url('edit.php?post_type=page'));
	}
	add_action('load-index.php', 'dashboard_redirect');
}



// remove quick edit in list posts/pages
function remove_quick_edit( $actions ) {
	unset($actions['inline hide-if-no-js']);
	return $actions;
}
add_filter('post_row_actions','remove_quick_edit',10,1);
add_filter('page_row_actions','remove_quick_edit',10,1);



// remove WP login logo
function my_custom_login_logo() {
	echo '<style type="text/css">
		#login{padding-top:150px;}
		.login h1{display:none;}
	</style>';
}
add_action('login_head', 'my_custom_login_logo');



// admin bar hide items
function my_css_adminimize() {
	global $post_type;
	?>
	<style>
		#wp-admin-bar-wp-logo,
		#wp-admin-bar-new-content,
		#wp-admin-bar-comments{
			display:none;
		}     
	</style>
	<?php
}
add_action('admin_head', 'my_css_adminimize');
