<?php

// custom image sizes
add_image_size('Square',640,640,true);



// prevent generate medium_large size
update_option('medium_large_size_w','0');
update_option('medium_large_size_h','0');



// remove [gallery] inline style
add_filter('use_default_gallery_style', '__return_false');



// remove special characters from uploaded media
function sanitize_media ($filename) {
	return remove_accents($filename);
}
add_filter('sanitize_file_name', 'sanitize_media', 10);



// deafault gallery image link to file
function my_gallery_default_type_set_link( $settings ) {
	$settings['galleryDefaults']['link'] = 'file';
	return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');