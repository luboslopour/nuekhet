<?php
function bootstrap_pagination($pages = '', $range = 1, $align = '', $style = '', $size = '') {
	global $paged;

	$showitems = ($range * 2) + 1;

	if(empty($paged)) {
		$paged = 1;
	}

	if($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages) {
			$pages = 1;
		}
	}

	if(!empty($size)) {
		$size = ' pagination-'.$size;
	}

	if(!empty($style)) {
		$bg = ' bg-'.$style;
		$text = ' text-'.$style;
		$border = ' border-'.$style;
	} else {
		$bg = '';
		$text = '';
		$border = '';
	}

	if(1 != $pages) {
        echo '<ul class="pagination'.$size.' justify-content-'.$align.'">';
	
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
			echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link(1).'" aria-label="First"><span aria-hidden="true">&laquo;</span><span class="sr-only">First</span></a></li>';
		}
	
	 	if($paged > 1 && $showitems < $pages) {
			echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link($paged - 1).'" aria-label="Previous"><span aria-hidden="true">&lsaquo;</span><span class="sr-only">Previous</span></a></li>';
	 	}

	 	if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
	 		echo '<li class="page-item disabled" aria-hidden="true"><span class="page-link">&hellip;</span></li>';
	 	}
	
		for ($i=1; $i <= $pages; $i++) {
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems)) {
				if($paged == $i) {
					echo '<li class="page-item active"><span class="page-link'.$bg.$border.'">'.$i.'<span class="sr-only">(current)</span></span></li>';
				} else {
					echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
				}
			}
		}

		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) {
			echo '<li class="page-item disabled" aria-hidden="true"><span class="page-link">&hellip;</span></li>';
		}
		
		if ($paged < $pages && $showitems < $pages) {
			echo '<li class="page-item d-none d-md-block"><a class="page-link'.$text.'" href="'.get_pagenum_link($paged + 1).'" aria-label="Next">'.__('Next').'<span class="sr-only">Next</span><span aria-hidden="true">&rsaquo;</span>&rsaquo;</a></li>'; 
		}

		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) {
			echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link($pages).'" aria-label="Last"><span class="sr-only">Last</span><span aria-hidden="true">&raquo;</span></a></li>';
		}
	
	 	echo '</ul>';
	}
}