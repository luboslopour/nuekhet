<?php

// $excerpt(10)
function excerpt($limit) {
	$permalink = get_permalink();
	$excerpt = explode(' ', get_the_excerpt(), $limit+1);
	if (count($excerpt)>=$limit+1) {
		array_pop($excerpt);
		$excerpt = implode(' ',$excerpt).'...';
	} else {
		$excerpt = implode(' ',$excerpt).'';
	}
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}
