<?php



// valid HTML5 | gallery shortcode
function html5_gallery($content){
    return str_replace('[gallery', '[gallery gallerytag="span" itemtag="span" icontag="span" captiontag="span"', $content);
}
add_filter('the_content', 'html5_gallery');



// valid HTML5 | remove table > border="0"
function table_border($content) {
    return str_replace('<table border="0"', '<table class="table"', $content);
}
add_filter('the_content', 'table_border');
