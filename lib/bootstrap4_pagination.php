<?php
/**
 * @package Bootstrap4_Pagination
 * @version 1.0
 */
/*
Plugin Name: Bootstrap4 Pagination
Description: Pagination for Bootstrap 4.
Version: 1.0
Author: lopour.net
Author URI: https://lopour.net
Text Domain: bootstrap4-pagination
Domain Path: /languages
License: GPL2
*/

function bootstrap4_pagination($range = 2, $size = false, $align = false, $style = false, $border = false) {
    global $paged;

    $showitems = ($range * 2) + 1;

    if(empty($paged)) {
        $paged = 1;
    }

    if($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages) {
            $pages = 1;
        }
    }

    if (in_array($size,array('sm','lg'))) {
        $size = ' pagination-'.$size;
    }

    if (in_array($align,array('start','center','end'))) {
        $align = ' justify-content-'.$align;
    }

    if (in_array($style,array('primary','secondary','success','info','warning','danger','light','dark'))) {
        $bg = ' bg-'.$style;
        $text = ' text-'.$style;
        $borderstyle = ' border-'.$style;
    } else {
        $bg = '';
        $text = '';
        $borderstyle = '';
    }

    if ($border=='top') {
        $border = ' border border-bottom-0 border-left-0 border-right-0 pt-4 mt-4';
    } elseif ($border=='bottom') {
        $border = ' border border-top-0 border-left-0 border-right-0 pb-4 mb-4';
    }

    if(1 != $pages) {
        echo '<nav role="navigation">';
        echo '<ul class="pagination'.$size.$align.$border.'">';
    
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
            echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link(1).'"><span>&laquo;</span><span class="sr-only">'.__('First','Walker','theme').'</span></a></li>';
        }
    
        if($paged > 1 && $showitems < $pages) {
            echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link($paged - 1).'"><span>&lsaquo;</span><span class="sr-only">'.__('Previous','Walker','theme').'</span></a></li>';
        }

        if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
            echo '<li class="page-item disabled"><span class="page-link">&hellip;</span></li>';
        }
    
        for ($i=1; $i <= $pages; $i++) {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems)) {
                if($paged == $i) {
                    echo '<li class="page-item active"><span class="page-link'.$bg.$borderstyle.'">'.$i.'<span class="sr-only">('.__('current','Walker','theme').')</span></span></li>';
                } else {
                    echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
                }
            }
        }

        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) {
            echo '<li class="page-item disabled"><span class="page-link">&hellip;</span></li>';
        }
        
        if ($paged < $pages && $showitems < $pages) {
            echo '<li class="page-item d-none d-md-block"><a class="page-link'.$text.'" href="'.get_pagenum_link($paged + 1).'"><span class="sr-only">'.__('Next','Walker','theme').'</span><span>&rsaquo;</span></a></li>'; 
        }

        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) {
            echo '<li class="page-item"><a class="page-link'.$text.'" href="'.get_pagenum_link($pages).'"><span class="sr-only">'.__('Last','Walker','theme').'</span><span>&raquo;</span></a></li>';
        }
    
        echo '</ul>';
        echo '</nav>';
    }
}