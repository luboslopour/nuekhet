<?php



// custom admin menu link for all settings
function all_settings_link() {
	add_options_page(__('All Settings'), __('All Settings'), 'administrator', 'options.php');
}
add_action('admin_menu', 'all_settings_link');



// custom admin menu
function remove_menu(){
	remove_menu_page('edit.php');
	remove_menu_page('edit-comments.php');
	remove_menu_page('link-manager.php');
	if (!current_user_can('manage_options')) {
		remove_menu_page('index.php');
		remove_menu_page('tools.php');
		remove_menu_page('wpcf7');
		remove_submenu_page('themes.php','themes.php');
		remove_submenu_page('themes.php','customize.php');
	}
}
add_action('admin_menu','remove_menu',999);



// custom menu order
function custom_menu_order($menu_ord) {
	if (!$menu_ord) return true;
	return array(
		'index.php',
		'edit.php?post_type=page',
		'edit.php?post_type=blog',
		'edit.php?post_type=feedback',
		'separator1',
		'edit.php',
		'separator2',
		'upload.php',
		'link-manager.php',
		'edit-comments.php',
		'themes.php',
		'plugins.php',
		'users.php',
		'tools.php',
		'options-general.php',
		'separator-last'
	);
}
add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');
