<?php

// allow custom menu
add_theme_support('menus');


// allow editor manage custom menu
get_role('editor')->add_cap( 'edit_theme_options' );


// register menus
function register_theme_menus() {
	register_nav_menus(
		array(
			'header-menu' => __('Header Menu','theme'),
			'header-buttons' => __('Buttons Menu','theme'),
		)
	);
}
add_action( 'init', 'register_theme_menus' );