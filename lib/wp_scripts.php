<?php
function wp_scripts_theme() {
	wp_deregister_script('wp-embed');
	wp_deregister_script('contact-form-7');
	wp_deregister_script('jquery');
	wp_register_script('jquery',get_bloginfo('template_url').'/dist/jquery/jquery.min.js',false,'3.2.1');
	wp_enqueue_script('jquery');
}
add_action('wp_print_scripts','wp_scripts_theme',100);

// remove type attribute
function script_type_attr($tag, $handle) {
	return preg_replace( "/ type=['\"]text\/javascript['\"]/", '', $tag );
}
add_filter('script_loader_tag','script_type_attr',10,2);