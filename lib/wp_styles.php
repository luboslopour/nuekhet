<?php
function wp_styles_theme() {
	wp_deregister_style('contact-form-7');
}
add_action('wp_print_styles','wp_styles_theme',100);

// remove type attribute
function style_type_attr($tag, $handle) {
	return preg_replace( "/ type=['\"]text\/css['\"]/", '', $tag );
}
add_filter('script_loader_tag','style_type_attr',10,2);