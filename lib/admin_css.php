<?php

// admin css
function admin_css() {

	echo '<link rel="stylesheet"  href="'.get_bloginfo('template_url').'/fonts/ionicons/css/ionicons.min.css" type="text/css" media="all" />';
	echo '<style>

	.acf-field-content-repeater .acf-field-section {
		background: #0073aa;
	}
	.acf-field-content-repeater .acf-fields.-left > .acf-field-section .acf-label {
		color: #fff;
	}
	.acf-field-content-repeater .acf-fields.-left > .acf-field-section:before {
		background: transparent;
	}



	.post-type-blog #postexcerpt .inside p,
	.post-type-feedback #postexcerpt .inside p {
		display: none;
	}
	
	.post-type-feedback #titlediv #title-prompt-text {
		color: transparent;
		letter-spacing: -.5em;
	}
	.post-type-feedback #titlediv #title-prompt-text:before {
		content: "'.__('Feedback author','admin-feedback-title','theme').'";
		color: #72777c;
		letter-spacing: 0;
	}
	.post-type-feedback #postexcerpt h2.hndle span {
		font-size: 0;
	}
	.post-type-feedback #postexcerpt h2.hndle:before {
		content: "'.__('Feedback','admin-feedback-excerpt','theme').'";
	}



	.acf-image-uploader img {
		max-width: 100px;
	}


	
	.acf-field-icon select,
	.select2-chosen,
	.select2-result {
		font-family:"Ionicons",sans-serif;
	}
	.select2-result {
		font-size: 1.2em;
	}



	</style>';
}
add_action('admin_head', 'admin_css');