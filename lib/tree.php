<?php

// is_tree(ID)
function is_tree( $pid ) {
	global $post; if ( is_page($pid) ) return true; $anc = get_post_ancestors( $post->ID );
	foreach ( $anc as $ancestor ) {
		if( is_page() && $ancestor == $pid ) {
			return true;
		}
	}
	return false;
}
