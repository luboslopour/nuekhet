gulp = require('gulp');
watch = require('gulp-watch');
sass = require('gulp-sass');
cleanCSS = require('gulp-clean-css');
autoprefixer = require('gulp-autoprefixer');
concat = require('gulp-concat');
rename = require('gulp-rename');
uglify = require('gulp-uglify');
gulpSequence = require('gulp-sequence');
webFontsBase64 = require('gulp-google-fonts-base64-css');


// fonts
gulp.task('fonts', function () {
	// googlefonts
	gulp.src('fonts.list')
	      .pipe(webFontsBase64())
	      .pipe(concat('googlefonts.scss'))
	      .pipe(gulp.dest('fonts/googlefonts/'));
	// ionicons
	gulp.src([
			'node_modules/ionicons/dist/**/*',
			'!node_modules/ionicons/dist/svg/',
			'!node_modules/ionicons/dist/svg/*'
		])
		.pipe(gulp.dest('fonts/ionicons/'));
});


// css build
gulp.task('css', function() {	
	gulp.src('scss/style.scss')
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(cleanCSS())
		.pipe(concat('style.min.css'))
		.pipe(gulp.dest('dist/css/'));
});


// js build
gulp.task('js', function() {
	gulp.src([
			'node_modules/bootstrap/dist/js/bootstrap.js',
			'js/custom.js'
		])
		.pipe(concat('script.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/js/'));
});


// copy
gulp.task('copy', function () {
	// jquery
	gulp.src('node_modules/jquery/dist/**/*')
		.pipe(gulp.dest('dist/jquery/'));
	// popper
	gulp.src('node_modules/popper.js/dist/**/*')
		.pipe(gulp.dest('dist/popper.js/'));
	// fancybox
	gulp.src('node_modules/@fancyapps/fancybox/dist/**/*')
		.pipe(gulp.dest('dist/fancybox/'));
	// plyr
	gulp.src('node_modules/plyr/dist/*')
		.pipe(gulp.dest('apps/plyr/'));
});


// gulp watch
gulp.task('watch',function(){
	gulp.watch(['scss/*.scss','fonts/*.scss',],['css','fonts']);
	gulp.watch('js/*.js',['js']);
});


// gulp build
gulp.task('build', gulpSequence('fonts','copy','css','js'))