<header class="col-lg-12 custom-home-main mb-5">
	<?php
		if(get_field('main-background','option')) {
			$background = get_field('main-background','option');
			echo '<div class="custom-home-bg" style="background-image:url('.$background['url'].')"></div>';
		}
	?>
	<div class="container">
		<div class="row">
			<?php
				if(get_field('main-image','option')) {
					$image = get_field('main-image','option');
					echo '<div class="col-lg-4 d-none d-lg-block cover-image" style="background-image: url('.$image['sizes']['large'].');"></div>';
				}
			?>
			<div class="<?php if(get_field('main-image','option')) {echo 'col-lg-8';} else {echo 'col-lg-12';} ?> align-self-center">
				
					<?php
						if(is_post_type_archive('blog')) {
							echo '
								<div class="bg-danger text-white py-4 px-5 my-4">
									<h1 class="display-3 text-white">
										<span class="ion ion-ios-chatbubbles-outline mr-3"></span>
										'.__('Blog','Page Title','theme').'
									</h1>
								</div>';
						} elseif(is_singular('blog')) {
							echo '
								<div class="bg-danger text-white py-4 px-5 my-4">
									<div class="display-3 text-white">
										<span class="ion ion-ios-chatbubbles-outline mr-3"></span>
											'.__('Blog','Page Title','theme').'
									</div>
								</div>';
						} elseif(is_post_type_archive('feedback')) {
							echo '
								<div class="bg-success text-white py-4 px-5 my-4">
									<h1 class="display-3 text-white">
										'.__('Feedback','Page Title','theme').'
									</h1>
								</div>';
						} elseif(is_404()) {
							echo '
								<div class="bg-danger text-white py-4 px-5 my-4">
									<h1 class="display-3 text-white">404</h1>
								</div>';
						} else {
							echo '
								<div class="bg-primary text-white py-4 px-5 my-4">
									<h1 class="display-3 text-white">'.get_the_title().'</h1>
								</div>';
						}
					?>
				
			</div>
		</div>
	</div>
</header>