		<footer class="bg-dark text-muted py-5" role="contentinfo">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 align-self-center">
						<h2 class="h4 m-0">
							<a href="" class="text-white"><?php bloginfo('name'); ?></a>
							<small>- <?php bloginfo('description'); ?></small>
						</h2>
						<p>&copy; <?php $y=date('Y'); if ($y>2010) { echo'2016 - '.$y; } else { echo $y; } ?> <?php bloginfo('name'); ?>. <?php _e('All rights reserved.','Footer','theme') ?></p>
						<?php
							if(get_field('phone','options') or get_field('email','options')) {
								echo '<p>';
									if(get_field('email','options')) {
										$email = get_field('email','options');
										echo '<a href="mailto:'.$email.'" class="d-inline-block text-white mr-4"><span class="ion ion-ios-mail-outline d-inline-block h2 text-muted m-0 pr-3 align-middle"></span>'.get_field('email','options').'</a>';
									}
									if(get_field('phone','options')) {
										echo '<span class="d-inline-block text-white"><span class="ion ion-ios-call-outline d-inline-block h2 text-muted m-0 pr-2 align-middle"></span>'.get_field('phone','options').'</span>';
									}
								echo '</p>';
							}
						?>
					</div>
					<div class="col-lg-4 align-self-center">
						<div class="h1 display-4 text-danger mb-3">Newsletter</div>
						<div class="input-group">
							<label for="newsletter-email" class="sr-only">E-mail Address</label>
							<input type="email" id="newsletter-email" class="form-control" placeholder="E-mail Address">
							<span class="input-group-btn">
								<button class="btn btn-secondary" type="button">Subscribe</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<script src="<?php bloginfo('template_url') ?>/dist/popper.js/umd/popper.min.js"></script>
		<script src="<?php bloginfo('template_url') ?>/dist/js/script.min.js"></script>
		<script src="<?php bloginfo('template_url') ?>/dist/fancybox/jquery.fancybox.min.js"></script>
		<?php wp_footer() ?>
	</body>
</html>