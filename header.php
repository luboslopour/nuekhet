<!doctype html>
<html <?php body_class('wordpress'); ?> <?php language_attributes() ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type') ?>;charset=<?php bloginfo('charset') ?>">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
		<title><?php echo bloginfo('name') ?><?php wp_title('-') ?></title>
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/dist/css/style.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/dist/fancybox/jquery.fancybox.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/fonts/ionicons/css/ionicons.min.css">
		<?php wp_head(); ?>
	</head>
	<body>
		<nav class="navbar navbar-dark navbar-expand-xl bg-dark fixed-top" role="navigation">
			<div class="container">
				<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
					<span class="d-inline-block align-middle mr-2 custom-logo-header">
						<?php @include 'img/logo/01.svg'; ?>
					</span>
					<<?php if(is_front_page()) {echo 'h1';} else { echo 'span';} ?> class="d-inline h5 m-0">
						<span class="d-none d-sm-inline text-uppercase"><?php bloginfo('name'); ?></span>
						<small class="d-none d-sm-inline text-muted">- <?php bloginfo('description'); ?></small>
					</<?php if(is_front_page()) {echo 'h1';} else { echo 'span';} ?>>
				</a>
				<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#mainmenu" aria-controls="mainmenu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="mainmenu">
					<?php
						wp_nav_menu(
							array(
								'theme_location'=>'header-menu',
								'container'=>'',
								'items_wrap'=>'<ul id="%1$s" class="navbar-nav ml-auto">%3$s</ul>',
								'walker' => new bs4navwalker(),
							)
						)
					?>
				</div>
				<?php
					wp_nav_menu(
						array(
							'theme_location'=>'header-buttons',
							'container'=>'',
							'items_wrap'=>'<ul id="%1$s" class="navbar-nav ml-auto">%3$s</ul>',
							'walker' => new bs4buttons(),
						)
					)
				?>
			</div>
		</nav>