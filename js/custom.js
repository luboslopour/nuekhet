// main navbar fixed
function navbar(){
	var navbar = $('.navbar.fixed-top').outerHeight();
	$('body').css({'padding-top':navbar});
	$('.custom-window-height').css({'min-height':'calc(100vh - '+navbar+'px)'});
}
navbar();
$(document).on('resize', function(){
	navbar();
})


// Bootstrap tooltip inicialize
$('[data-toggle=tooltip]').tooltip();

// Bootstrap popover inicialize
$('[data-toggle=popover]').popover();

// fancybox
$('a[href$=".gif"],a[href$=".jpg"],a[href$=".jpeg"],a[href$=".png"],a[href$=".GIF"],a[href$=".JPG"],a[href$=".JPEG"],a[href$=".PNG"]').attr('data-fancybox','');
$('.gallery').each(function(){
	id = $(this).attr('id');
	$('a',this).attr('data-fancybox',id); 
});