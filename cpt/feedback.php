<?php

function register_cpt_feedback() {
	$labels = array(
		'name'                => _x( 'Feedback', 'Post Type General Name', 'theme' ),
		'singular_name'       => _x( 'Feedback', 'Post Type Singular Name', 'theme' ),
		'menu_name'           => __( 'Feedback', 'theme' ),
		'name_admin_bar'      => __( 'Post','add new from admin bar'), // or 'Page'
		'all_items'           => __( 'All Posts' ), // or 'All Pages'
		'add_new'             => _x( 'Add New', 'post'), // or 'Add New', 'page'
		'add_new_item'        => __( 'Add New Post'), // or 'Add New Page'
		'edit_item'           => __( 'Edit Post'), // or 'Edit Page'
		'new_item'            => __( 'New Post'), // or 'New Page'
		'view_item'           => __( 'View Post'), // or 'View Page'
		'search_items'        => __( 'Search Posts'), // or 'Search Pages'
		'not_found'           => __( 'No posts found.'), // or 'No pages found.'
		'not_found_in_trash'  => __( 'No posts found in Trash.'), // or 'No pages found in Trash.'
	);
	$rewrite = array(
	    'slug'                => _x( 'feedback', 'Post Type Slug', 'theme' ),
	);
	$args = array(
		'supports'            => array( 'title','excerpt' ),
		'menu_icon'           => 'dashicons-admin-comments',
		'menu_position'       => 5,
		'labels'              => $labels,
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page'
	);
	register_post_type( 'feedback', $args );
}
add_action( 'init', 'register_cpt_feedback', 0 );



// hide admin permalink
function admin_feedback_hide__permalinks($return, $post_id, $new_title, $new_slug, $post) {
	if($post->post_type=='feedback') {
		return '';
	}
	return $return;
}
add_filter('get_sample_permalink_html', 'admin_feedback_hide__permalinks', 10, 5);