<?php
	get_header();
	get_template_part('header','main');
?>
<div class="container text-center py-5">
	<h2 class="my-5"><?php _e('Error 404! That page can&rsquo;t be found.','theme'); ?></h2>
	<p class="my-5"><?php _e('It looks like nothing was found at this location.','theme'); ?></p>
</div>
<?php get_footer() ?>
