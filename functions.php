<?php

// Multilingual theme
load_theme_textdomain('theme',get_template_directory().'/languages');

// Post thumbnail
add_theme_support('post-thumbnails',array('blog'));

// Custom Post Types
require('cpt/blog.php');
require('cpt/feedback.php');

// excerpt
require('lib/excerpt.php');
// valid html5
require('lib/html5.php');
// admin menu
require('lib/admin_menu.php');
// images / gallery
require('lib/images.php');
// adminimize
require('lib/adminimize.php');
// wp_head
require('lib/wp_head.php');
// wp_scripts
require('lib/wp_scripts.php');
// wp_styles
require('lib/wp_styles.php');
// tinymce
require('lib/tinymce.php');
// custom nav menu
require('lib/nav_menu.php');
// custom nav menu
require('lib/admin_css.php');
// BS4 pagination
require('lib/bootstrap4_pagination.php');
// ACF theme options
require('acf/theme_options.php');
// ACF front page
require('acf/front_page.php');
// ACF front page
require('acf/template_contact.php');

// BS4 Navbar walker
require('walker/bs4navwalker.php');
// BS4 Button walker
require('walker/bs4buttons.php');

if(!current_user_can('manage_options')){
	// HTML Minify
	require('lib/html_minify.php');
}