<?php
	get_header();
	get_template_part('header','main');
?>
<div class="py-5">
	<div class="container">
		<main>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="row">
					<div class="col-lg-8 offset-lg-2">
						<?php the_content(); ?>
					</div>
				</div>
			<?php endwhile; ?>
		</main>
	</div>
</div>
<?php get_footer(); ?>
