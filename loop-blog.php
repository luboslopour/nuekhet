<?php while(have_posts()) : the_post(); ?>
	<article class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-4 offset-lg-0 mb-4">
		<?php if(has_post_thumbnail()): ?>
			<figure>
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail('thumbnail',['class'=>'img-fluid']); ?>
				</a>
			</figure>
		<?php endif; ?>
		<h2 class="h5 text-muted m-0"><?php the_title(); ?></h2>
		<p class="mb-2 small text-muted"><span class="ion ion-ios-calendar-outline mr-2"></span><?php the_time('d. m. Y') ?></p>
		<?php echo '<p>'.get_the_excerpt().'</p>'; ?>
		<nav class="my-3">
			<a class="btn btn-outline-danger btn-sm" href="<?php the_permalink(); ?>"><?php _e('Read more','Continue reading','theme'); ?></a>
		</nav>
	</article>
<?php endwhile; ?>